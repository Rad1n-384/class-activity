package geometry;

public class LotsOfShape {
    public static void main(String[] args) {
        Shape[] shapesArray = new Shape[5];
        // shapesArray[0] = new Shape();

        shapesArray[0] = new Rectangle(4, 5);
        shapesArray[1] = new Rectangle(3, 6);
        shapesArray[2] = new Circle(2.5);
        shapesArray[3] = new Circle(3.0);
        shapesArray[4] = new Square(4);
        for (int i = 0; i < shapesArray.length; i++) {
            System.out.println(
                    "Shape area: " + shapesArray[i].getArea() + " Shape perimeter: " + shapesArray[i].getPerimeter());
        }
    }
}
