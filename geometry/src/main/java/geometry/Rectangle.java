package geometry;

public class Rectangle implements Shape {
    private double side1;
    private double side2;

    public Rectangle(double side1, double side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    public double getArea() {
        double area = side1 * side2;
        return area;
    }

    public double getPerimeter() {
        double perimeter = 2 * (side1 + side2);
        return perimeter;
    }
}
